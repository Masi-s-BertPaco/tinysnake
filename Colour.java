import java.io.Serializable;

public enum Colour implements Serializable
{
    WALL,BACKGROUND,SNAKE_HEAD,SNAKE_BODY,FRUIT;
}
