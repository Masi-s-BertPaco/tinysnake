import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IGameEngine extends Remote{

    /**
     * All methods in commentary are not used on the CLient side
     * @throws RemoteException
     */
   
    public void resetGame() throws RemoteException;

    public void updateGUI() throws RemoteException;

    public boolean moveForward() throws RemoteException;

    /**
     * Spawn a new fruit
     */
    public void nextFruit() throws RemoteException;

    // game_engine_timer_methods
    public void adjustTimerValue() throws RemoteException;

    public void rescheduleTimer() throws RemoteException;

    public void stopTimer() throws RemoteException;


    /**
     * Start the game engine
     *
     **/
    public void start() throws RemoteException;//

    public void exit(int exitStatus) throws RemoteException;

    // Events Management
    public void treatEvent(GameEvent event) throws RemoteException;

    public void updateScore() throws RemoteException;

    // Getters
    public String getPlayerName() throws RemoteException;

    public PlayArea getPlayArea() throws RemoteException;

    public PlayerInfo getPlayerInfo()  throws RemoteException;

    public boolean isGameOver() throws RemoteException;

    public int getPlayerScore() throws RemoteException;

    public Coord getCurrentFruit() throws RemoteException;

    // Setters

    public boolean setDirection(Direction d) throws RemoteException;

    public void setPlayerInfo(PlayerInfo pinfo) throws RemoteException;

    public void setPlayerName(String name) throws RemoteException;

    public void setGui(IGUI gui) throws RemoteException;

    public String areaOfPlaytoString() throws RemoteException;


    //register and unregister callbacks
    public String sayHello( ) throws java.rmi.RemoteException;

    // This remote method allows an object client to 
    // register for callback
    // @param clientObject is a reference to the
    //        object of the client; to be used by the server
    //        to make its callbacks.

    public void registerForCallback(IGUI clientObject) throws java.rmi.RemoteException;

    // This remote method allows an object client to 
    // cancel its registration for callback

    public void unregisterForCallback( IGUI clientObject) throws java.rmi.RemoteException;
}
