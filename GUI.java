
//--> GUI_class
import javax.swing.*;

import java.awt.*;
import java.awt.event.*;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.Hashtable;

/**
 * Core Class of the GUI
 */
public class GUI extends UnicastRemoteObject implements IGUI, Serializable {
	/**
	 *
	 */
	private static final long serialVersionUID = 6475998389405915400L;

	final private IGameEngine ge;
	private static JFrame frame;
	private JPanel gamePane;
	private JPanel settingPane;
	private PlayBoard playBoard;
	private JButton playButton;
	private JButton doneButton;
	private JLabel playerName;
	private JLabel playerScore;
	private JTextField nameField;
	private JTextField passwordField;
	private PlayArea board;
	JButton bouton_sign = new JButton("Sign In");
	JLabel nom = new JLabel("name");
	JLabel password = new JLabel("password");
	JLabel confirm_password = new JLabel("confirm_password");
	JTextField name = new JTextField(10);
	JTextField passwords = new JTextField(10);
	JTextField confirms_password = new JTextField(10);
	JButton btn_connexion= new JButton("btn_connexion");
	String Name = name.getText();
    String Password = password.getText();
    Hashtable<String, String> Info_Joueurs = new Hashtable<String,String>();
	private int nbPartialRefresh = 0;
	/**
	 * this function allows new users to register 
	 * then his name and password are saved in a HasHmap.
	 * @see Register
	 */
	@Override
    public void Register() {
    	frame.setTitle(" ");	
    	JPanel panel = new JPanel();
    	panel.setLayout(new FlowLayout());
    	JPanel contentPane = (JPanel) frame.getContentPane();
		frame.setSize(700, 300);
		contentPane.setLayout(new FlowLayout());
		nom.setFont(new java.awt.Font(Font.SERIF,Font.BOLD,10));
		password.setFont(new java.awt.Font(Font.SERIF,Font.BOLD,10));
         btn_connexion.setFont(new java.awt.Font(Font.SERIF,Font.BOLD,10));
		
		confirm_password.setFont(new java.awt.Font(Font.SERIF,Font.BOLD,10));
		panel.add(nom);
		panel.add(name);
		
		panel.add(password);
	    panel.add(passwords);
	    panel.add(confirm_password );
        panel.add(confirms_password );
                
        btn_connexion.addActionListener(new ActionListener() {public void actionPerformed(ActionEvent event) 
        {
        	
        		
        		
     
        	
          		 if((name.getText().length()==0)) {
          			 JOptionPane.showMessageDialog(nom, "THE NAME BOX CAN'T BE EMPTY", "ERROR",JOptionPane.INFORMATION_MESSAGE);
          		 }
          		 else if((passwords.getText().length()==0)) {
          			 JOptionPane.showMessageDialog(passwords, "THE PASSWORDS BOX CAN'T BE EMPTY", "ERROR",JOptionPane.INFORMATION_MESSAGE);
          		 }
          		 else if(confirms_password.getText().length()==0){
          			 JOptionPane.showMessageDialog(confirms_password,"THE confirms_password BOX CAN'T BE EMPTY", "ERROR" ,JOptionPane.INFORMATION_MESSAGE);
          		 }
          		 else if(!passwords.getText().equals(confirms_password.getText())){
          			 JOptionPane.showMessageDialog(confirms_password, "PASSWORDS DON'T MATCH", "ERROR",JOptionPane.INFORMATION_MESSAGE);
          		 }
          	      else {JOptionPane.showMessageDialog(confirm_password,"Successfully create an account");
          	    
          	              Info_Joueurs.put(Name,Password);
          	        try {
          	        	
						showGamePane();
					} catch (RemoteException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
          	        try {
						update();
					} catch (RemoteException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
          	        }
          		
          		
          	}
	    });
		panel.add(btn_connexion);
		frame.setLocationRelativeTo(null);
		frame.addWindowListener(new WindowAdapter() {
	    	public void windowClosing(java.awt.event.WindowEvent e) {
	    		int bouton_croix = JOptionPane.showConfirmDialog(GUI.frame, "DO YOU REALLY WANT TO QUIT?", "CLOSE WINDOW", JOptionPane.YES_NO_OPTION);
	    	if(bouton_croix == JOptionPane.YES_OPTION) {
	    		GUI.frame.dispose();
	    	}}
	    });
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		frame.setContentPane(panel);
		frame.setVisible(true);
    }

	/**
	 * Creates a new GUI based on a Game Engine, a size of a square and the numer of
	 * cases in a side.
	 *
	 * @param ge           The IGameEngine is the interface of GameEngine that
	 *                     manages the events, the moves of the snake...
	 * @param caseSize     The size of a single square in the GUI
	 * @param casesPerSide The length of the PlqyArea side given in number of
	 *                     squqres
	 *
	 * @see IGameEngine
	 * @see PlayArea
	 */
	public GUI(IGameEngine ge, final int caseSize, final int casesPerSide) throws RemoteException {
		super();
		// to make GUI creation thread-safe
		this.ge = ge;
		board = new PlayArea(casesPerSide);
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				try {
					createGUI(caseSize, casesPerSide);
				} catch (RemoteException e) {
					System.out.println("Problem to create the graphical user interface");
					e.printStackTrace();
				}
			}
		});
	}

	public static void main(String[] args) throws MalformedURLException, NotBoundException {
		try {
			IGameEngine ge = (IGameEngine)Naming.lookup("//localhost/SnakeServer");
			String  playerName = "Tester";

			if (args.length == 1)
				if (args[0].length() > 15)
					playerName = args[0].substring(0, 15);
				else
					playerName = args[0];
					ge.setPlayerInfo(new PlayerInfo(playerName));

			
			
			GUI gui = new GUI(ge, 20, 25);
			ge.registerForCallback(gui);
			ge.setGui((IGUI)java.rmi.server.RemoteObject.toStub(gui));

		} catch (RemoteException e) {
			e.printStackTrace();
			System.out.println("Error at startup");
			System.exit(1);
		}catch (NotBoundException e) {
			e.printStackTrace();
			System.out.println("The server seems to be down");
		}catch (MalformedURLException e) {
			e.printStackTrace();
			System.out.println("The url is ill-formatted");
		}
	}

    /**
	 * Refresh the GUI: mostly performs partial refresh, but refresh the full array
	 * at regular intervals
	 * 
	 * @throws RemoteException
	 */
	public void update() throws RemoteException 
    {
        
        if (nbPartialRefresh == 0)
        {
	        board.copy(ge.getPlayArea());
        }
        else
        {
            ge.getPlayArea().updateCopy(board);
        }

        nbPartialRefresh = (nbPartialRefresh + 1) % Settings.FULL_REFRESH_INTERVAL;
        
	    javax.swing.SwingUtilities.invokeLater(new Runnable() {
		    public void run() 
            {
				String score="-1";
				try {
					score = Integer.toString(ge.getPlayerScore());
					playerScore.setText(fillString(score, 8, '0'));
		        	frame.repaint();
				} catch (RemoteException e) {
					System.out.println("Problem to get the score and refresh the window");

					e.printStackTrace();
				}

		        
		    }
	    });
    }

    /**
	 * GUI creation, this method is called by the constructor
	 *
	 * @param caseSize     The size of a single square in the GUI
	 * @param casesPerSide The length of the PlayArea side given in number of
	 *                     squares
	 * @throws RemoteException
	 */
	public void createGUI(int caseSize, int casesPerSide) throws RemoteException 
    {

	    frame = new JFrame("Tiny Snake");

	    // menus
	    JMenuBar menuBar = new JMenuBar();
	    JMenu menu = new JMenu("File");
	    JMenuItem menuItem = new JMenuItem("New Game");
	    menuItem.addActionListener(new GameStarter(this));
	    menu.add(menuItem);
	    menuItem = new JMenuItem("Settings");
	    menuItem.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent event) 
            {
		        try {
					showSettingPane();
				} catch (RemoteException e) {
					System.out.println("Problem to show the settings pane");
					e.printStackTrace();
				}
		    }
	    });
	    menu.add(menuItem);
	    menuItem = new JMenuItem("Exit");
	    menuItem.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent event) 
            {
		        try {
					getGameEngine().exit(0);
				} catch (RemoteException e) {
					System.out.println("Problem to exit the window");
					e.printStackTrace();
				}
		    }
	    });
	    menu.add(menuItem);
	    menuBar.add(menu);
	    frame.setJMenuBar(menuBar);

	    buildGamePaneWidgets(caseSize, casesPerSide);
	    buildSettingPaneWidgets();

	    showSettingPane();
	    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    frame.setLocation(100, 100);
	    frame.setVisible(true);
    }

    /**
     * Creates the pane that will display the board
     *
     * @param caseSize The size of a single square in the GUI
     * @param casesPerSide The length of the PlayArea side given in number of squares
     */
    public void buildGamePaneWidgets(int caseSize, int casesPerSide) 
    {
	    playBoard = new PlayBoard(caseSize, casesPerSide, this);
	    //NextPiecePanel npc = new NextPiecePanel(caseSize / 2, this);
	    playBoard.addKeyListener(new KeyboardHandler(this));

	    // init game pane
	    JPanel scorePanel = new JPanel();
	    scorePanel.setLayout(new BoxLayout(scorePanel, BoxLayout.Y_AXIS));
	    // other components for labels here
	    nameField = new JTextField(15);
	    passwordField = new JTextField(15);
	    playerName = new JLabel();
	    playerScore = new JLabel(fillString("0", 8, '0'));
	    scorePanel.add(playerName);
	    scorePanel.add(playerScore);

	    playButton = new JButton("Play");
	    playButton.setMnemonic(KeyEvent.VK_P);
	    playButton.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent event) 
            {
		        try {
					ge.start();
					
				} catch (RemoteException e) {
					System.out.println("Problem to start the game");

					e.printStackTrace();
				}

				disablePlayButton();
		       	playBoard.requestFocus();
		        
		    }
	    });
	    JPanel bottomPanel = new JPanel(new FlowLayout());
	    bottomPanel.add(new ExitButton(this));
	    bottomPanel.add(playButton);

	    gamePane = new JPanel(new BorderLayout());
	    gamePane.setOpaque(true);
	    gamePane.add(playBoard, BorderLayout.CENTER);
	    gamePane.add(scorePanel, BorderLayout.EAST);
	    gamePane.add(new ExitButton(this), BorderLayout.PAGE_END);
	    gamePane.add(bottomPanel, BorderLayout.PAGE_END);
    }

    /**
	 * Creates the Settings Pane
	 * 
	 * @throws RemoteException
	 */
	public void buildSettingPaneWidgets() throws RemoteException 
    {
	    // init setting/welcome pane
	    settingPane = new JPanel(new GridLayout(0, 2));
	    settingPane.setOpaque(true);
	    // settingPane.add(new JLabel);
	    nameField = new JTextField(15);
	    if (ge.getPlayerName() != null)
	        nameField.setText(ge.getPlayerName());
	    doneButton = new JButton("Done");
	    doneButton.setMnemonic(KeyEvent.VK_D);
	    frame.getRootPane().setDefaultButton(doneButton);
	    doneButton.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent event) 
            {
		        if (nameField.getText().length()== 0) {
		        	JOptionPane.showMessageDialog(nameField, "THE NAME BOX CAN'T BE EMPTY", "ERROR",JOptionPane.INFORMATION_MESSAGE);
		        }
		        else if(passwordField.getText().length()==0) {
		        	JOptionPane.showMessageDialog(passwordField, "THE PASSWORDS BOX CAN'T BE EMPTY", "ERROR",JOptionPane.INFORMATION_MESSAGE);}
		        else  {
		        	 try {
					showGamePane();
					update();
				} catch (RemoteException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		                 }
					
				
		        
		    }
	    });

	    settingPane.add(new JLabel("TinySnake"));
	    settingPane.add(new JLabel());
	    settingPane.add(new JLabel("Your name:"));
	    settingPane.add(nameField);
	    settingPane.add(new ExitButton(this));
	    settingPane.add(doneButton);
    }

    /**
	 * Toggle display of the full game pane
	 * 
	 * @throws RemoteException
	 */
	public void showGamePane() throws RemoteException 
    {
	    playerName.setText(ge.getPlayerName());
	    frame.setContentPane(gamePane);
	    frame.getRootPane().setDefaultButton(playButton);
	    playBoard.requestFocus();
	    frame.pack();
    }

    /**
	 * Toggle display of the setting pane
	 * 
	 * @throws RemoteException
	 */
	public void showSettingPane() throws RemoteException 
    {
	    frame.setContentPane(settingPane);
	    frame.getRootPane().setDefaultButton(doneButton);
	    nameField.setText(ge.getPlayerName());
	    frame.pack();
    }

    public void disablePlayButton() { playButton.setEnabled(false); }
    public void enablePlayButton() { playButton.setEnabled(true); }

    /**
     * Copy a String in a buffer and fill the buffer with padding
     *
     * @param origString The string to copy
     * @param totalSize The size of the buffer
     * @param pad The padding character
     */
    public String fillString(String origString, int totalSize, char pad) 
    {
	    StringBuffer stringFill = new StringBuffer();
	    for (int i = origString.length(); i <= totalSize; i++)
	        stringFill.append(pad);

	    return stringFill.toString().concat(origString);
    }

    public IGameEngine getGameEngine() 
    {
	    return ge;
    }

    public PlayArea getBoard() 
    {
	    return board;
    }

	public String notifyMe(String message) throws RemoteException {
		
		String returnMessage = "Call back received: " + message;
		System.out.println(returnMessage);
		return returnMessage;
	}
}

// --<
// --> playboard_class

/**
 * The PlayBoard representation in the GUI
 */
class PlayBoard extends JPanel 
{
	private static final long serialVersionUID = 6475998389405915400L;

    private int caseSize;

    private int casesPerSide;

    private GUI gui;

    /**
     * Creates the PlayBoard panel based on single square size and lengh of the PlayArea side
     * in number of squares
     *
     * @param caseSize The single square size
     * @param casesPerSide The length of PlayArea side
     */
    public PlayBoard(int caseSize, int casesPerSide, GUI gui) 
    {
	    super();
	    int sideSize = (caseSize - 1) * casesPerSide + 1;
	    setPreferredSize(new Dimension(sideSize, sideSize));
	    this.caseSize = caseSize;
	    this.casesPerSide = casesPerSide;
	    this.gui = gui;
    }

    /**
     * Print the contents of the board on Graphic output
     *
     * @param g the graphic output
     */
    public void paintComponent(Graphics g) 
    {
	    PlayArea playArea = gui.getBoard();
	    super.paintComponent(g);
	    g.setColor(ColourMapper.map(Colour.BACKGROUND));
	    g.fillRect(0, 
		        0, 
		        (caseSize - 1) * casesPerSide + 1, 
		        (caseSize - 1) * casesPerSide + 1);

	    int pixCoeff = caseSize - 1;

	    for (Coord pos : playArea) {
	        int pixX = pos.getCol() * pixCoeff;
	        int pixY = pos.getLine() * pixCoeff;
	        Colour caseColour = playArea.getCaseColour(pos);
	        if (caseColour != Colour.BACKGROUND) 
            {
		        new BoardCase(pixX, pixY, ColourMapper.map(caseColour)).drawCase(g);
	        }
	    }
	    try {
			if (gui.getGameEngine().isGameOver())
				gameOver(g);
		} catch (RemoteException e) {
			System.out.println("Not able to stop, due to game over case");
			e.printStackTrace();
		}
    }

    /**
     * Print GameOver on screen
     *
     * @param g the graphic output
     */
    private void gameOver(Graphics g) 
    {
	    g.setColor(Color.MAGENTA);
	    g.setFont(new Font(null, Font.BOLD, 50));
	    g.drawString("GAME OVER",
		        (int) ((casesPerSide / 2.0f - 8.0) * (caseSize - 1)),
		        (int) ((casesPerSide / 2.0f + 0.5) * (caseSize - 1)));
	    gui.enablePlayButton();
    }

    /**
     * Represents a case to draz
     */
    class BoardCase 
    {
	    int pixX;
	    int pixY;
	    Color fill;

        /**
         * Create a new case base on top left corner coordinate and fill colour
         *
         * @param pixX the abscissa of the top left corner
         * @param pixY The ordinate of the top left corner
         * @param fill The fill colour of the square
         */
	    public BoardCase(int pixX, int pixY, Color fill) {
	        this.pixX = pixX;
	        this.pixY = pixY;
	        this.fill = fill;
	    }

	    public void drawCase(Graphics g) {
	        g.setColor(fill);
	        g.fillRect(pixX + 1, pixY + 1, caseSize - 2, caseSize - 2);
	    }

    }
}

// --<
// --> next_piece_panel_class

/**
 * The Exit Button
 */
class ExitButton extends JButton
{
	private static final long serialVersionUID = 6475998389405915400L;

    //final private GUI gui;

    ExitButton(final GUI gui) 
    {
	    super("Exit");
	    //this.gui = gui; 
	    setMnemonic(KeyEvent.VK_E);
	    addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent event) 
            {
		        try {
					gui.getGameEngine().exit(0);

				} catch (RemoteException e) {
					System.out.println("Not able to exit window");
					e.printStackTrace();
				}
		    }
	    });
    }
}


/**
 * The Keyboard Handler
 */
class KeyboardHandler extends KeyAdapter 
{
    private IGameEngine ge;

    public KeyboardHandler(GUI gui) 
    {
	    ge = gui.getGameEngine();
    }

    /**
     * Transforn a keyboard event into a GameEvent
     *
     * @param e The keyboard event
     */
    public void keyPressed(KeyEvent e) {

	    switch (e.getKeyCode()) {
	        case KeyEvent.VK_UP:
	            try {
					ge.treatEvent(GameEvent.UP);
				} catch (RemoteException e1) {
					System.out.println("Problem for treating UP movement");
					e1.printStackTrace();
				}
	            break;
	        case KeyEvent.VK_DOWN:
	            try {
					ge.treatEvent(GameEvent.DOWN);
				} catch (RemoteException e1) {
					System.out.println("Problem for treating DOWN movement");
					e1.printStackTrace();
				}
	            break;
	        case KeyEvent.VK_LEFT:
	            try {
					ge.treatEvent(GameEvent.LEFT);
				} catch (RemoteException e1) {
					System.out.println("Problem for treating LEFT movement");
					e1.printStackTrace();
				}
	            break;
	        case KeyEvent.VK_RIGHT:
	            try {
					ge.treatEvent(GameEvent.RIGHT);
				} catch (RemoteException e1) {
					System.out.println("Problem for treating RIGHT movement");
					e1.printStackTrace();
				}
	            break;
	    }

    }

    public void keyReleased(KeyEvent e) {}
    public void keyTyped(KeyEvent e) {}
}


/**
 * Game Starter
 */
class GameStarter implements ActionListener {

    private GUI gui;

    public GameStarter(GUI gui) {
	    this.gui = gui;
    }

    public void actionPerformed(ActionEvent e) {
	    try {
			gui.showGamePane();
		} catch (RemoteException e1) {
			System.out.println("Problem for showing game pane");
			e1.printStackTrace();
		}
		gui.disablePlayButton();
		
	    try {
			gui.getGameEngine().start();
		} catch (RemoteException e1) {
			System.out.println("Problem to start the game engine");
			e1.printStackTrace();
		}
    }
}

/**
 * Simple Colour Mappep
 */
class ColourMapper {
    /**
     * Return a GUI readable color based on Colour enumeration
     *
     * @param colour The value of the Colour Enumeration to translate
     */ 
    public static Color map(Colour colour) {
	    switch(colour) {
	        case FRUIT: return Color.RED;
	        case SNAKE_BODY: return Color.GREEN;
	        case SNAKE_HEAD: return Color.YELLOW;
	        case WALL: return Color.DARK_GRAY;
	        case BACKGROUND: 
	        default: return Color.LIGHT_GRAY;
	    }
    }
}

