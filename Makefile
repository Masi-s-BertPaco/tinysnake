all: TinySnake.jar

TinySnake.jar: Colour.class Coord.class Direction.class GameEngine.class IGameEngine.class GameEvent.class GUI.class IGUI.class PlayArea.class PlayerInfo.class Rectangle.class Settings.class Snake.class TinySnake.class
	jar -cvfm $@ manifest.mf *.class
	
%.class: %.java
	javac -Xlint $<
	
clean: clean-linux clean-windows

clean-linux:
	rm *.jar
	rm *.class

clean-windows:
	del *.jar
	del *.class

cc-doc: doc
	javadoc -d doc *.java

doc:
	mkdir $@

TinySnake.tar.gz: Makefile manifest.mf Colour.java Coord.java Direction.java GameEngine.java IGameEngine.java GameEvent.java GUI.java IGUI.java PlayArea.java PlayerInfo.java Rectangle.java Settings.java Snake.java TinySnake.java
	tar -cvzf $@ $^
run-app: run-server
	
run-client:
	java GUI
run-server:
	java TinySnake