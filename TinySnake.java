import java.rmi.*;
import java.rmi.server.*;
import java.rmi.registry.*;
import java.net.*;
import java.io.*;
import java.util.*;

public class TinySnake
{
	static String registryURL;
    public static void main(String[] args)
    {

		try {
			Properties props = System.getProperties();
			props.setProperty("java.rmi.server.codebase", "http://localhost");
			props.setProperty("java.security.policy", "server.policy");
			startRegistry(1099);
			GameEngine exportedObj = new GameEngine(25);

			registryURL= "//localhost/SnakeServer";
			Naming.rebind(registryURL, exportedObj);
			System.out.println("Server is ready");
		/* String playerName = "Tester";

		if (args.length == 1)
			if (args[0].length() > 15)
				playerName = args[0].substring(0, 15);
			else
				playerName = args[0];
				exportedObj.setPlayerInfo(new PlayerInfo(playerName));

			GUI gui = new GUI(exportedObj, 20, 25);
			exportedObj.setGui(gui); */
		} catch (Exception e) {
			System.out.println("Exception in TinySnake.main: "+e);
		}

		
	}
	

	private static void startRegistry(int RMIPortNum) throws RemoteException{

		try {
			Registry registry = LocateRegistry.getRegistry(RMIPortNum);
			registry.list();

		} catch (RemoteException e) {
			Registry registry = LocateRegistry.createRegistry(RMIPortNum);


		}

	}


	
}
