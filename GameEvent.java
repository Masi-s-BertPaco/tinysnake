import java.io.Serializable;

public enum GameEvent implements Serializable
{
    TIMEOUT, UP, DOWN, LEFT, RIGHT, FLY
}
