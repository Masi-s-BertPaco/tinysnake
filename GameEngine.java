import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.*;

public class GameEngine extends UnicastRemoteObject implements IGameEngine
{
    /**
     *
     */
    private static final long serialVersionUID = 6475998389405915400L;
    private PlayArea playboard;
    private Snake player;
    private PlayerInfo pinfo;
    private Random seed;
    private boolean gameOver;
    private Coord currentFruit;
    private int nbEmptyCases;
    private IGUI gui;

    private int timerValueChangeInterval;
    private int timerValue;
    private int minTimerValue;
    private float timerScaleFactor;

    private Timer timer;
    private TimerTask pendingTask;

    private int fruitValue;
    private boolean growth;

    private Vector clientList;

    public GameEngine(int size) throws RemoteException
    {
        this(size, size);

    }

    public GameEngine(int width, int length) throws RemoteException
    {

        super();
        clientList = new Vector();
        this.playboard = new PlayArea(width, length);
        this.seed = new Random(System.currentTimeMillis());

        resetGame();
    }

    public void resetGame() {
        this.player = new Snake(playboard.getMiddle(), Direction.LEFT);
        playboard.init();

        for (Coord c : player.getPosition()) {
            playboard.setCaseColour(c, player.getBodyColour());
        }

        playboard.setCaseColour(player.getHeadPosition(), player.getHeadColour());

        gameOver = false;
        nbEmptyCases = (playboard.getLength() - 2) * (playboard.getHeight() - 2) - player.getSnakeLength();

        fruitValue = Settings.INIT_FRUIT_VALUE;
        growth = false;

        timer = new Timer();
        timerValueChangeInterval = Settings.NB_FRUIT_SPEEDUP;
        timerValue = Settings.INIT_TIMER_VALUE;
        timerScaleFactor = Settings.TIMER_SCALE;
        minTimerValue = Settings.MIN_TIMER_VALUE;
    }

    public void updateGUI() throws RemoteException
    {
        try {
            gui.update();
        } catch (RemoteException e) {
        System.out.println("Not able to update the graphical interface. CLient has probably left");        
        resetGame();
        }
    }

    public boolean moveForward()
    {
        boolean safeMove;

        playboard.setCaseColour(player.getHeadPosition(), player.getBodyColour());
        if (!growth)
            playboard.setCaseColour(player.getTailPosition(), Colour.BACKGROUND);

        player.moveForward(growth);
        safeMove = playboard.caseIsSafe(player.getHeadPosition());

        growth = false;

        playboard.setCaseColour(player.getHeadPosition(), player.getHeadColour());

        return safeMove;
    }

    /**
     * Spawn a new fruit
     **/
    public void nextFruit() {
        int selectedCase = seed.nextInt(nbEmptyCases);
        int counter = 0;
        for (Coord c : playboard)
        {
            if (counter == selectedCase && playboard.caseIsEmpty(c))
            {
                currentFruit = c;
                playboard.setCaseColour(c, Colour.FRUIT);
                break;
            }
            
            if (playboard.caseIsEmpty(c))
                counter++;   
        }

        nbEmptyCases--;
        adjustTimerValue();
    }

    // game_engine_timer_methods
    public void adjustTimerValue() 
    {
	    if (player.getSnakeLength() % timerValueChangeInterval == 0) 
        {
	        float nextTimerValue = (float) timerValue / timerScaleFactor;
	        timerValue = nextTimerValue < minTimerValue ? 
		        minTimerValue : 
		        Math.round(nextTimerValue);
            fruitValue += Settings.FRUIT_INCREASE;
	    }
    }

    public void rescheduleTimer() 
    {
	    stopTimer();
	    pendingTask = new TimerTask() 
        {
		    public void run() {
		        synchronized (GameEngine.this) 
                {
			        try {
                        treatEvent(GameEvent.TIMEOUT);
                    } catch (RemoteException e) {
                        System.out.println("Problem to pass to the event handeling");
                        e.printStackTrace();
                    }
		        }
		    }
	    };
	    timer.schedule(pendingTask, timerValue);
    }

    public void stopTimer() {
	    if (pendingTask != null)
	        pendingTask.cancel();
    }


    /**
     * Start the game engine
     * 
     * @throws RemoteException
     *
     **/
    public void start() throws RemoteException {
        resetGame();
        nextFruit();
        updateGUI();
	    rescheduleTimer();
    }

    public void exit(int exitStatus)
    {
        System.exit(exitStatus);
    }

    // Events Management
    synchronized public void treatEvent(GameEvent event) throws RemoteException {
	    if (gameOver)
	        return;

	    switch (event) {
	        case TIMEOUT:
                if (moveForward())
                {
		            rescheduleTimer();
                    if (currentFruit.equals(player.getHeadPosition()))
                    {
                        updateScore();
                        nextFruit();
                        growth = true;
                    }
                }
                else
                {
                    timer.cancel();
                    gameOver = true;
                }
		        updateGUI();
                break;
	        case UP:
                player.setDirection(Direction.UP);
	            break;
	        case DOWN:
                player.setDirection(Direction.DOWN);
	            break;
	        case RIGHT:
                player.setDirection(Direction.RIGHT);
	            break;
	        case LEFT:
                player.setDirection(Direction.LEFT);
	            break;
	        default:
	            // should never occur
	    }
    }

    public void updateScore() {
        pinfo.addToScore(fruitValue);
    }

    // Getters
    public String getPlayerName()
    {
        return pinfo.getName();
    }

    public PlayArea getPlayArea() 
    {
        return playboard;
    }

    public PlayerInfo getPlayerInfo() 
    {
        return pinfo;
    }

    public boolean isGameOver()
    {
        return gameOver;
    }

    public int getPlayerScore() 
    {
        return pinfo.getScore();
    }

    public Coord getCurrentFruit() 
    {
        return currentFruit;
    }

    // Setters
    public boolean setDirection(Direction d)
    {
        return player.setDirection(d);
    }

    public void setPlayerInfo(PlayerInfo pinfo)
    {
        this.pinfo = pinfo;
    }

    public void setPlayerName(String name)
    {
        pinfo.setName(name);
    }
    // This is how the client registers itself
    public void setGui(IGUI gui)
    {
        this.gui = gui;
    }

    public String areaOfPlaytoString()
    {
        return playboard.areaOfPlaytoString();
    }





    public String sayHello( )   
    throws java.rmi.RemoteException {
      return("hello");
  }

  public synchronized void registerForCallback(
    IGUI callbackClientObject)
    throws java.rmi.RemoteException{
      // store the callback object into the vector
      if (!(clientList.contains(callbackClientObject))) {
         clientList.addElement(callbackClientObject);
      System.out.println("Registered new client ");
      doCallbacks();
    } // end if
  }  

// This remote method allows an object client to 
// cancel its registration for callback
// @param id is an ID for the client; to be used by
// the server to uniquely identify the registered client.
  public synchronized void unregisterForCallback(
    IGUI callbackClientObject) 
    throws java.rmi.RemoteException{
    if (clientList.removeElement(callbackClientObject)) {
      System.out.println("Unregistered client ");
    } else {
       System.out.println(
         "unregister: clientwasn't registered.");
    }
  } 

  private synchronized void doCallbacks( ) throws java.rmi.RemoteException{
    // make callback to each registered client
    System.out.println(
       "**************************************\n"
        + "Callbacks initiated ---");
    for (int i = 0; i < clientList.size(); i++){
      System.out.println("doing "+ i +"-th callback\n");    
      // convert the vector object to a callback object
      IGUI nextClient = 
        (IGUI)clientList.elementAt(i);
      // invoke the callback method
        nextClient.notifyMe("Number of registered clients="
           +  clientList.size());
    }// end for
    System.out.println("********************************\n" +
                       "Server completed callbacks ---");
  } // doCallbacks
}
