import java.rmi.*;

/**
 * Interface of the GUI
 */
public interface IGUI extends Remote{

    /**
	 * Refresh the GUI: mostly performs partial refresh, but refresh the full array
	 * at regular intervals
	 * 
	 * @throws RemoteException
	 */
    public void update() throws RemoteException ;
    
    /**
         * GUI creation, this method is called by the constructor
         *
         * @param caseSize     The size of a single square in the GUI
         * @param casesPerSide The length of the PlayArea side given in number of
         *                     squares
         * @throws RemoteException
         */
        public void createGUI(int caseSize, int casesPerSide) throws RemoteException;

    /**
         * Creates the pane that will display the board
         *
         * @param caseSize The size of a single square in the GUI
         * @param casesPerSide The length of the PlayArea side given in number of squares
         */
        public void buildGamePaneWidgets(int caseSize, int casesPerSide) throws RemoteException;

    /**
         * Creates the Settings Pane
         * 
         * @throws RemoteException
         */
        public void buildSettingPaneWidgets() throws RemoteException;

    /**
         * Toggle display of the full game pane
         * 
         * @throws RemoteException
         */
        public void showGamePane() throws RemoteException;


    /**
         * Toggle display of the setting pane
         * 
         * @throws RemoteException
         */
        public void showSettingPane() throws RemoteException;
    /**
         * Disable play button
         * 
         * @throws RemoteException
         */
        public void disablePlayButton() throws RemoteException;
    /**
         * Enable play button
         * 
         * @throws RemoteException
         */
        public void enablePlayButton() throws RemoteException;

    /**
         * Copy a String in a buffer and fill the buffer with padding
         *
         * @param origString The string to copy
         * @param totalSize The size of the buffer
         * @param pad The padding character
         */
        public String fillString(String origString, int totalSize, char pad) throws RemoteException;

    /**     This remote method is invoked by a callback
        server to make a callback to an client which
        implements this interface.
        @param message - a string containing information for the
                         client to process upon being called back. 
        */

        public String notifyMe(String message) throws java.rmi.RemoteException;
        
        public void Register() throws RemoteException;
}

